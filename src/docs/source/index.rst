**awftp** (|release|)
=====================

**A FTP-style client for HCP Anywhere File Sync & Share**

HCP Anywhere is a File Sync & Share solution developed by Hitachi
Vantara. It offers a wide range of FS&S clients for Desktop computers,
Mobile devices as well as Browsers.
A lacking piece so far is a CLI client that allows to access HCP Anywhere
on devices where no GUI is available (Linux servers, for example).

**awftp** tries to fill this gap by providing a look and feel as close as
possible to well-known CLI-based ftp clients. The features available
are a subset of what is offered by ftp clients, due to the functionality the
HCP Anywhere FS&S service offers through its API; some other functions in
ftp clients simply doesn't make sense for use with HCP Anywhere. On the other
hand, there are some features (*snap*, for example) not found with ftp...

**Features**

    *   Works with any FS&S API version >= 2.1.1 while using the highest
        version known
    *   Navigate the folders stored in HCP Anywhere, including Mobilized NAS,
        Shared Folders, Team Folders, Backup Folders
    *   List folders content, including deleted files/folders
    *   Store, update and retrieve files
    *   Move files and folders
    *   Undelete files/folders
    *   Create/delete folders (even recursive)
    *   Link handling:

        *   Create links to share content with others - internal/public,
            read-only, read-write, write-only
        *   List links
        *   Delete links
        *   Work with folders shared with us:

            *   List all collaboration folders
            *   List invitations
            *   Accept invitations
            *   Reject invitations

    *   Work with snapshots

        *   List available snapshots
        *   Provide view into snapshots
        *   Restore files from snapshots

    *   Dynamically enables/disables features depending on the HCP Anywhere
        version connected to


..  toctree::
    :maxdepth: 2

    15_installation
    20_run
    40_commands
    90_todos
    97_changelog
    98_license


