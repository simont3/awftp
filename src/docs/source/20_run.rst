Run **awftp**
=============

..  Tip::

    Make sure you have set your system language to something that understands
    **UTF-8** encoding, as you might run into display issues with filenames
    being coded in UTF-8 (from a Mac, for example).

    ::

        $ export LC_ALL=en_US.UTF-8


Depending on how you installed **awftp**, you can start it on various ways:

*   When using the provided binary (and given that you placed it somewhere in
    your ``$PATH``)::

        $ awftp <anywhere.your.domain>

*   If you installed **awftp** through ``pip install awftp``::

        $ awftp <anywhere.your.domain>

*   If you cloned the git repository:;

        $ python3 awftp.py <anywhere.your.domain>


Arguments
---------

::

    $  awftp --help
    usage: awftp.py [-h] [--version] [-d] [--idp] [--api {2.1.1,3.0.0,3.1.0}] \
                    [-r CMDFILE] [--nossl] [aw_server]

    positional arguments:
      aw_server             the HCP Anywhere system to connect ([user[:password]@]anywhere.your.domain)

    options:
      -h, --help            show this help message and exit
      --version             show program's version number and exit
      -d                    enable debugging output (ugly and a bit chatty!)
      --idp                 list available identity providers and exit
      --api {2.1.1,3.0.0,3.1.0}
                            force using a specific FS&S API version
      -r CMDFILE            run commands in CMDFILE; in case this shall be w/o interaction
                            entirely, don't forget ``bye`` as the last command!
      --nossl               disable SSL (most likely, this won't work, as HCP Anywhere requires
                            SSL encryption)


Authentication
--------------

**awftp** will authenticate the user against the Active Directory HCP Anywhere is
integrated with.

Proxy
-----

In case there is a proxy needed to connect to HCP Anywhere, make sure you have
set the required shell variables. You will need to have ``https_proxy``, at
least. `See here for an explanation
<https://www.gnu.org/software/wget/manual/html_node/Proxies.html>`_.
