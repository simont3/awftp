Installation
============

Using a local Python 3 installation
-----------------------------------

Dependencies
^^^^^^^^^^^^

You need to have at least Python 3.7 installed to run **awftp**.

It uses the famous
`requests package <http://docs.python-requests.org/en/master/>`_
for communication with HCP Anywhere. And it uses
`click <http://click.pocoo.org/6/>`_, for the progress bar and console output.

.. Tip::

    It's suggested to use a virtual environment to fence the dependency
    from your primary Python environment.


Installing on a local Python 3
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Make sure you have Python 3.7 (or better) installed**

In case it's not installed, get it here:
`<https://www.python.org/downloads/>`_.

There are two ways to install **awftp**:

1.  System-wide

    *   Install **awftp** by running:

        ..  code-block:: text
            :emphasize-lines: 1

            $ pip install awftp

    -or-

    *   Get the source from
        `gitlab.com <https://gitlab.com/simont3/awftp>`_\ ,  either

        *   by downloading the source archive, or

        *   by cloning the repository:

            ..  code-block:: text
                :emphasize-lines: 1

                $ git clone https://gitlab.com/simont3/awftp


    *   Install locally, including the dependency:

        ..  code-block:: text
            :emphasize-lines: 1

            $ python setup.py install


2.   In a virtual environment

    **Linux / Mac OS X / etc.**

    *   Create a fresh virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ python3 -m venv .awftp

    *   Activate the virtual environment:

        ..  code-block:: text
            :emphasize-lines: 1

            $ source .awftp/bin/activate

    *   Update the base tools:

        ..  code-block:: text
            :emphasize-lines: 1

            (.awftp) $ pip install -U pip setuptools
            Requirement already up-to-date: pip in ./.awftp/lib/python3.6/site-packages
            Collecting setuptools
              Downloading setuptools-36.0.1-py2.py3-none-any.whl (476kB)
            Installing collected packages: setuptools
              Found existing installation: setuptools 28.8.0
                Uninstalling setuptools-28.8.0:
                  Successfully uninstalled setuptools-28.8.0
            Successfully installed setuptools-36.0.1

    *   Install **awftp**:

        ..  code-block:: text
            :emphasize-lines: 1

            (.awftp) $ pip install awftp
            Processing awftp-1.1.0-py3-none-any.whl
            Collecting requests>=2.13.0 (from awftp==1.1.0)
              Downloading requests-2.18.1-py2.py3-none-any.whl (88kB)
            Collecting click>=6.7 (from awftp==1.1.0)
              Using cached click-6.7-py2.py3-none-any.whl
            Collecting beautifulsoup4>=4.6.0 (from awftp==1.1.0)
              Using cached beautifulsoup4-4.6.0-py3-none-any.whl
            Collecting chardet<3.1.0,>=3.0.2 (from requests>=2.13.0->awftp==1.1.0)
              Using cached chardet-3.0.4-py2.py3-none-any.whl
            Collecting certifi>=2017.4.17 (from requests>=2.13.0->awftp==1.1.0)
              Using cached certifi-2017.4.17-py2.py3-none-any.whl
            Collecting idna<2.6,>=2.5 (from requests>=2.13.0->awftp==1.1.0)
              Using cached idna-2.5-py2.py3-none-any.whl
            Collecting urllib3<1.22,>=1.21.1 (from requests>=2.13.0->awftp==1.1.0)
              Using cached urllib3-1.21.1-py2.py3-none-any.whl
            Installing collected packages: chardet, certifi, idna, urllib3, requests, click, beautifulsoup4, awftp
            Successfully installed awftp-1.1.0 beautifulsoup4-4.6.0 certifi-2017.4.17 chardet-3.0.4 click-6.7 idna-2.5 requests-2.18.1 urllib3-1.21.1

    Now you can run **awftp** as long as you have the virtual environment
    activated:

    ..  code-block:: text
        :emphasize-lines: 1

        (.awftp) $ awftp user:password@anywhere.your.domain
        About to connect to anywhere.your.domain.
        User user logged in.
        Remote system type is HCP Anywhere, FS&S API is v3.0.0
        Using binary mode to transfer files.
        awftp>


