ToDos
=====

Not yet implemented commands
----------------------------

Copy files remotely::

    awftp> cp remote-source-file remote-target-file


Work with multiple files at once::

    awftp> prompt
    awftp> mput local-files
    awftp> mget remote-files
    awftp> mrm remote-files


Not yet implemented features
----------------------------

*   regular expressions for file handling commands (``ls``, ``get``, ``put``,
    ``rm``).

    That means, behavior should be similar what you expect if you run
    ``ls somefolder/a*.txt`` and alike.

