Commands
========


..  Tip::

    *   **Most commands support output redirection using "|", ">" and ">>".**

    *   **File names containing spaces must either:**

        *   **be placed in quotation marks**
        *   **or blanks need to be masked with a backslash**

    *   **Running commands can be cancel by pressing CTRL-C**


bye
----

::

    awftp> bye

Terminate awftp session and exit.


cd
----

::

    awftp> cd [remote-directory]

Change remote working directory to remote-directory, if given, otherwise to /


cdup
----

::

    awftp> cdup

Change remote working directory to parent directory.


close
-----

::

    awftp> close

Terminate the session with HCP Anywhere, but stay in awftp.


clear
-----

::

    awftp> clear

Clear the screen.


coll
----

::

    awftp> coll [-b] [-t] [-s]

Collaboration: list folders shared with us
    | ``-b`` backup folders
    | ``-t`` team folders
    | ``-s`` shared folders


dir
----

::

    awftp> dir [-1] [-d] [remote-path]

List contents of remote path
    | ``-1`` display one name per line
    | ``-d`` show deleted files, too


exit
----

::

    awftp> exit

Terminate awftp session and exit


find
----

::

    awftp> find [-1] [-d] [-s <snap_id>] <pattern>

Search for files/folders containing <pattern> in their name,
starting at the current directory (in realtime or the active
snapshot).

    | ``-1`` (digit one) display one name per line
    | ``-d`` search for deleted files, too
    | ``-s`` search in snapshot <snap_id> (from snap -l)

<pattern> is the string to search for, min. 3 characters


get
----

::

    awftp> get remote-file [local-file]

Receive (download) a file.


help
----

::

    awftp> help [command] | ? [command]

List the available commands or show help for the named command.


hist
----

::

    awftp> hist <filename>

Show the history of a file.


invacc
------

::

    awftp> invacc [-s] <Id>

Accept an outstanding share invitation.
    | ´´-s´´ enable sync to desktop clients
    | ´´<Id>´´ the invitation Id (obtain by using the ´´invls´´ command)


invls
-----

::

    awftp> invls

List outstanding share invitations.


invrej
------

::

    awftp> invrej <Id>

Reject an outstanding share invitation.
    | ´´<Id>´´ the invitation Id (obtain by using the ´´invls´´ command)


lcd
----

::

    awftp> lcd [local-directory]

Change the local working dirkectory to local-directory (or to home directory,
if local-directory isn't given).


link
----

::

    awftp> link [-a] [-i|-p] -r|-u|-ru [expiration_days] file|folder
    awftp> link -d <id>

Create a link to share a file or folder
    | ``-a`` add an access code
    | ``-i`` force creating an internal link
    | ``-p`` force creating a public link
    | ``-r`` the link is good for view and download of files
    | ``-u`` the link allows to upload into the linked folder
    |    (at least one of -r and -u is required)

If expiration_date is not given, an unlimited link will be created

Delete an existing link
    | ``-d`` delete link with <id>, where <id> is the leftmost integer
      displayed by the *links* command


links
-----

::

    awftp> links

List all active links



ls
----

::

    awftp> ls [-1] [-d] [remote-path]

List contents of remote path
    | ``-1`` display one name per line
    | ``-d`` show deleted files, too
    | ``-u`` show names URL-encoded

    Example::

        awftp> ls
        M drw     0.00   B 2017/05/12 12:21:24 home on NAS2
        B dr-     0.00   B 2017/05/12 20:34:40 macth
        S dr-     0.00   B 2017/05/13 21:12:07 sharedfolder_ro
        S drw     0.00   B 2017/05/13 21:12:06 sharedfolder_rw
        T drw     0.00   B 2017/05/13 21:04:26 teamfolder
        - drw     0.00   B 2017/05/12 12:23:43 test
        - -rw     4.64 KiB 2017/05/13 21:16:21 testfile.txt

    The first character per line declares the type of the entry as being (or
    belonging to):

        | **B**: a backup folder
        | **M**: a mobilized NAS share
        | **S**: a shared folder
        | **T**: a team folder


lls
----

::

    awftp> lls [local-path]

List contents of local path


lpwd
----

::

    awftp> lpwd

Print the local working directory.


ls
----

::

    awftp> ls [-d] [remote-path]

List contents of remote path.
    | ``-d`` show deleted files


mkdir
-----

::

    awftp> mkdir [-R] directory-name

Make directory on the remote machine.
    | ``-R`` recursively make parent directories as well.


mv
----

::

    awftp> mv [-R] old_name new_name

Move a file or directory to a new name or position.
    | ``-R`` recursively make parent directories if required.


open
----

::

    awftp> open [[user[:password]@]hcpanywhere-name]

Connect to an HCP Anywhere server

**Be aware that there is a history file - think if you want to store
your password in it...**


progress
--------

::

    awftp> progress

Toggle progress meter off/on.


put
----

::

    awftp> put [-u] local-file [remote-file]

Send one file.
    | ``-u`` update an existing file


pwd
----

::

    awftp> pwd

Print the remote working directory.


quit
----

::

    awftp> quit

Terminate awftp session and exit.


restore
-------

::

    awftp> restore [-d] remote-name

Make the version of a file or folder within the active snapshot the current
version (restore it to "now").

    | ``-d`` restore a deleted file


rm
----

::

    awftp> rm [-d [-R]] remote-name

Remove a remote file or folder
    | ``-d`` remove a folder
    | ``-R`` recursively delete a folder and *all* its content.

rmdir
-----

::

    awftp> rmdir [-R] directory-name

Remove a directory on the remote machine.

    | ``-R`` recursively delete a the directory and *all* its content.

..  versionadded:: 1.3.4

run
----

::

    awftp> run <commandfile>

Run a batch of commands stored in <commandfile>.


search
------

::

    awftp> search [-1] [-d] [-s <snap_id>] <pattern>

Search for files/folders containing <pattern> in their name,
starting at the current directory (in realtime or the active
snapshot)

    | ``-1`` (digit one) display one name per line
    | ``-d`` search for deleted files, too
    | ``-s`` search in snapshot <snap_id> (from snap -l)

<pattern> is the string to search for, min. 3 characters


snap
----

::

    awftp> snap -l | -s <index> | -u

Work with restore points (snapshots).
    | ``-l`` list available snapshots
    | ``-s <index>`` work on this snapshot
    | ``-u`` unset snapshot (return to "now")

Once a snapshot has been set, all operations will be based on it.'


status
------

::

    awftp> status

Show the session status.

time
----

::

    awftp> time command [args]*

Run *command* as usual, but print the processing time afterwards.


user
----

::

    awftp> user

Get information about the user\'s settings in HCP Anywhere.
